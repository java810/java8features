package com.sneha.java8.features;

/**
 * Invoked by MultiInteritnceTest
 *
 */
public interface MultiInteritnceRight {

	public void m1();

	public void m2();

}
