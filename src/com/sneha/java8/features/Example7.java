package com.sneha.java8.features;
//Second example of without lamda----When there was no lamda expression
//Square of numbers --invoved by Test3.java
public class Example7 implements FunctionInterfaceLamdaExpressionEg3{

	@Override
	public int squareit(int x) {
		
		return x*x;
		
	}

}
