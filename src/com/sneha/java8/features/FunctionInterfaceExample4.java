package com.sneha.java8.features;
//inheritance example in functional interface
@FunctionalInterface
public interface FunctionInterfaceExample4 extends FunctionInterfaceExample1 {
	
//Perfect valid functional interface 
//Parent Abstract method is applicable for child interface hence child also has Single Abstract method
//We cannot add Abstract Method in child ,then child will have two Abstract Method ,then it will not be a functional Interface

}
