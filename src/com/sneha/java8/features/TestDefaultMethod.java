package com.sneha.java8.features;

public class TestDefaultMethod implements DefaultMethodinIntrfceEg {

	@Override
	public void m1() {
		System.out.println("Im the implementation of abstarct method m1 in DefaultMethodinIntrfceEg");

	}

	@Override
	public void m2() {
		System.out.println("Im the implementation of abstarct method m2 in DefaultMethodinIntrfceEg");

	}

	/*
	 * While overriding default method no need to use keyword as default for the
	 * method because within a class default keyword has different meaning if you
	 * declare default you will get compile time error modifier default not allowed
	 * here
	 */
	public void m3() {
		System.out.println("overiding version of default method m3 ");
	}

	public static void main(String[] args) {

		TestDefaultMethod t = new TestDefaultMethod();
		t.m1();
		t.m2();
		t.m3(); // using overridden implementation defined in this class
		t.m4();// using the default implementation available in the interface

	}

}
