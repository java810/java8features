package com.sneha.java8.features;
/*
 * 1)From lambda expression we can access enclosing class variables and method variables directly.
 * 2)The Local variables referenced from lambda expression are implicitly final and hence
 *    we can't  perform re-assignment otherwise we get compile time error
 */
public class LamdaExpressionVariablesConceptEg2 {
int x=666;
public void m2() {
	int y=777;
	FunctionInterfaceExample1 i= () -> {
		System.out.println(x);//Accessing class variable from Lambda Expression
		System.out.println(y);//Accessing method variable from Lambda Expression
		//y=888; --- Compile time error 
	};
	i.m1();
	
}
	public static void main(String[] args) {
		LamdaExpressionVariablesConceptEg2 t= new LamdaExpressionVariablesConceptEg2();
		t.m2();

	}

}
