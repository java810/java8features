package com.sneha.java8.features;

import java.util.ArrayList;
import java.util.Collections;
public class CollectionwithoutLambdaExample {

	public static void main(String[] args) {
		ArrayList<Integer> l= new ArrayList<Integer>();
		l.add(20);
		l.add(10);
		l.add(30);
		l.add(50);
		System.out.println("Default Insertion Order: "+l);
	//We are using MyComparator.java class for sorting in ascending order
		Collections.sort(l, new MyComparator()); //Passing list and new object of MyComparator.java
		System.out.println("Sorting in acending Order: "+l);
		

	}

}
