package com.sneha.java8.features;
//Without lamda Expression
public class Test1 {

	public static void main(String[] args) {
		Example3 example3 = new Example3();
		//this also can be written as :
		FunctionInterfaceExample1 i= new Example3();/*Parent holding child reference*/
		
		example3.m1();
		i.m1();

	}

}
