package com.sneha.java8.features;
//Lamda Expression Example
//How to do example 5 using lamda expression 
public class Example6 {

	public static void main(String[] args) {
		//complier can guess the type automatically no need to explicitly specify  (int a,int b) as functional interface has one one method automatically the type is int for that method in thos case
		FunctionInterfaceLamdaExpressionEg2 eg2= (a,b) -> System.out.println(a+b);
		eg2.sum(2, 3);
		eg2.sum(3, 3);

	}

}
