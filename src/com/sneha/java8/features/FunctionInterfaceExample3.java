package com.sneha.java8.features;
//method overriding in functional interface
@FunctionalInterface
public interface FunctionInterfaceExample3 extends FunctionInterfaceExample1 {
	
	public void m1();
	//Perfectly valid as though it looks like child class has two abstract  methods but not is not true
	/*
	 * Method name is same in parent and child
	 * hence child is overriding parent abstract method ,therefore only one abstract method in child 
	 * hence child is also a functinal Interface
	 */

}
