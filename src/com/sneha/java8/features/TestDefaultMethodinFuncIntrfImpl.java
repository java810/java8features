package com.sneha.java8.features;

public class TestDefaultMethodinFuncIntrfImpl {

	public static void main(String[] args) {

		DefaultMethodinFunctionalIntrfceEg i = () -> System.out.println("Im Lambda Expression");
		i.m1();
		i.m3(); // Using default implementation
		i.m4(); // Using default implementation
	}

}
