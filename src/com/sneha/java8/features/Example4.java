package com.sneha.java8.features;

//Lamda Expression Example
/*
 * Nameless
 * No Access Modifiers 
 * No Return Type
 * 
 * if single statement curly braces can be removed
 * 
 */
public class Example4 {

	public static void main(String[] args) {
		// No implementation class required separately for the interface
		FunctionInterfaceLamdaExpressionEg1 i = () -> System.out.println("Im am lamda Expressiom");
		i.lamdaexpressionexampleabstractmethod();

	}

}
