package com.sneha.java8.features.predifinedfunctionalIntrf.unaryoperator;

import java.util.function.IntUnaryOperator;

/**
 * -Child to Function
 * -If Input and Output Type is the same then we should go unary operator rather than Function.
 *  E.g: Function<Integer,Integer> is same as UnaryOperator<Integer>
 * -Primitive Unary Operator-IntUnaryOpertor -- if both input is integer
 * -eg: UnaryOperator<Integer> -> IntUnary
 *
 */
public class Example2 {

	public static void main(String[] args) {
		
		IntUnaryOperator iuo= i->i*i;
		System.out.println(iuo.applyAsInt(10));
		
	}

}
