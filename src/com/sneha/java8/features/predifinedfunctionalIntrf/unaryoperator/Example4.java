package com.sneha.java8.features.predifinedfunctionalIntrf.unaryoperator;

import java.util.function.DoubleUnaryOperator;

/**
 * -Child to Function
 * -If Input and Output Type is the same then we should go unary operator rather than Function.
 *  E.g: Function<Double,Double> is same as UnaryOperator<Double>
 * -Primitive Unary Operator-DoubleUnaryOpertor -- if both input is Long
 * -eg: UnaryOperator<Double> -> DoubleUnary
 *
 */
public class Example4 {

	public static void main(String[] args) {
		
		DoubleUnaryOperator dup = i->i*i;
		System.out.println(dup.applyAsDouble(100.00));

	}

}
