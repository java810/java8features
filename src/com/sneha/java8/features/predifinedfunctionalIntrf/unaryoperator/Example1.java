package com.sneha.java8.features.predifinedfunctionalIntrf.unaryoperator;

import java.util.function.UnaryOperator;

/**
 * -Child to Function
 * -If Input and Output Type is the same then we should go unary operator rather than Function.
 *  E.g: Function<Integer,Integer> is same as UnaryOperator<Integer>
 *
 */
public class Example1 {

	public static void main(String[] args) {
	UnaryOperator<Integer> up=i->i*i;
	System.out.println(up.apply(10));

	}

}
