package com.sneha.java8.features.predifinedfunctionalIntrf.unaryoperator;

import java.util.function.LongUnaryOperator;

/**
 * -Child to Function
 * -If Input and Output Type is the same then we should go unary operator rather than Function.
 *  E.g: Function<Long,Long> is same as UnaryOperator<Long>
 * -Primitive Unary Operator-LongUnaryOpertor -- if both input is Long
 * -eg: UnaryOperator<Long> -> LongUnary
 *
 */
public class Example3 {

	public static void main(String[] args) {
		
		LongUnaryOperator luo= i->i*i;
		
		//input and output type both is long
		System.out.println(luo.applyAsLong(100));

	}

}
