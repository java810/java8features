package com.sneha.java8.features.predifinedfunctionalIntrf.bifunction;

import java.util.ArrayList;
import java.util.function.BiFunction;

/**
 * Takes two input argument and one return type wap to create employee object
 *
 */
public class Example1 {

	public static void main(String[] args) {
		BiFunction<String, Integer, Employee> bf = (a, b) -> new Employee(a, b);
		System.out.println(bf.apply("sneha", 10));
		System.out.println();

		ArrayList<Employee> l = new ArrayList<Employee>();
		l.add(bf.apply("Kaushi", 10));
		l.add(bf.apply("Kavya", 20));
		l.add(bf.apply("Sweta", 30));

		for (Employee e : l) {
			System.out.println(e);
			System.out.println();
		}
	}

}
