package com.sneha.java8.features.predifinedfunctionalIntrf.bifunction;

public class Employee {

	String Empname;
	Integer EmpId;

	public Employee(String Empname, Integer EmpId) {
		
		this.Empname = Empname;
		this.EmpId = EmpId;
	}

	@Override
	public String toString() {
		return "Employee [Empname=" + Empname + ", EmpId=" + EmpId + "]";
	}
}
