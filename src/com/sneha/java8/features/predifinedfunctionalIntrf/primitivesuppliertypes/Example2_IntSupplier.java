package com.sneha.java8.features.predifinedfunctionalIntrf.primitivesuppliertypes;

import java.util.function.IntSupplier;

/**
 * Does not take any input but returns int value
 * Method name - getAsInt()
 *
 */
public class Example2_IntSupplier {

	public static void main(String[] args) {
		IntSupplier is = () -> {
			return 10;
		};
		System.out.println(is.getAsInt());
	}

}
