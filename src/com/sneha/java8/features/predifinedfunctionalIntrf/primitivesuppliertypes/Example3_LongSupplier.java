package com.sneha.java8.features.predifinedfunctionalIntrf.primitivesuppliertypes;

import java.util.function.LongSupplier;

/**
 * Does not take any input but returns Long value
 * Method Name- getAsLong()
 *
 */
public class Example3_LongSupplier {

	public static void main(String[] args) {
		
           LongSupplier ls= ()->{
        	   return 100;
           };
           
           System.out.println(ls.getAsLong());
	}

}
