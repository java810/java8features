package com.sneha.java8.features.predifinedfunctionalIntrf.primitivesuppliertypes;

import java.util.function.BooleanSupplier;

/**
 * Does not take any input but returns boolean value
 * Method Name-> getAsBoolean()
 *
 */
public class Example1_BooleanSupplier {

	public static void main(String[] args) {
		BooleanSupplier bs= ()->10%2==0;
		System.out.println(bs.getAsBoolean());

	}

}
