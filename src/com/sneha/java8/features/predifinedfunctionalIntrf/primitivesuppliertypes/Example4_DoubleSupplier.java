package com.sneha.java8.features.predifinedfunctionalIntrf.primitivesuppliertypes;

import java.util.function.DoubleSupplier;

/**
 * Does not take any input but returns Long value 
 * Method Name- getAsDouble()
 *
 */
public class Example4_DoubleSupplier {

	public static void main(String[] args) {
		DoubleSupplier ds = () -> {
			return 100.00;
		};

		System.out.println(ds.getAsDouble());
	}

}
