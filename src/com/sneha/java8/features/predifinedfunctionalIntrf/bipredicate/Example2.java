package com.sneha.java8.features.predifinedfunctionalIntrf.bipredicate;

import java.util.Scanner;
import java.util.function.BiPredicate;

public class Example2 {

	public static void main(String[] args) {
		
		BiPredicate<Integer, Integer> bp = (a, b) -> (a + b) % 2 == 0;
		
		//take input from user
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter First Value:");
		int a=sc.nextInt();
		System.out.println("Enter Second Value:");
		int b =sc.nextInt();
		sc.close();
		
		
		if (bp.test(a, b)) {
			System.out.println("Sum of both numbers is even");
		} else {
			System.out.println("Sum of both numbers is odd");
		}
	}

}
