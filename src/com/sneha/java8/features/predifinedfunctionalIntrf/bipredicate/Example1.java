package com.sneha.java8.features.predifinedfunctionalIntrf.bipredicate;

import java.util.function.BiPredicate;

/**
 * -Conditional check if number of input arguments are two
 *
 */
public class Example1 {

	public static void main(String[] args) {
		BiPredicate<Integer, Integer> bp = (a, b) -> {
			System.out.println("Sum of two numbers: " + (a + b));
			return true;
		};

		bp.test(1, 2);
	}

}
