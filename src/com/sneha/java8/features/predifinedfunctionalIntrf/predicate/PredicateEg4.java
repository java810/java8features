package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

import java.util.function.Predicate;

/**
 * 
 * Print Strings who's length greater than or equal to 5
 */
public class PredicateEg4 {

	public static void main(String[] args) {

		String[] names = { "Nag", "Sneha", "RekhaM", "Motilal", "Puja", "SikhaT", "Sia", "Kauslendra" };
		Predicate<String> p = s->s.length()>=5; 
		System.out.println("Strings who's length greater than or equal to 5:");
		for(String s:names) {
			if(p.test(s)) {		
				System.out.println(s+" ");
			}
		}
		
		
	}

}
