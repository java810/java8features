package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;


import java.util.ArrayList;
import java.util.function.Predicate;

public class PredicateEg5 {

	public static void main(String[] args) {

		Predicate<Employee> p = e -> e.EmpId > 1000 ;
		ArrayList<Employee> l = new ArrayList<Employee>();
		l.add(new Employee("Sneha", 1000));
		l.add(new Employee("Neha", 2000));
		l.add(new Employee("Puja", 300));
		l.add(new Employee("Sujana", 40));
		l.add(new Employee("Shreya", 5000));
		System.out.println("Employee Id is of employee's who's id is greater than 1000:");
		for (Employee e:l) {
			if(p.test(e)) {
				System.out.println(e);
			}
		}
	
	}

}
