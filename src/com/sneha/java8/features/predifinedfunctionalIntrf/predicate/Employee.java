package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

public class Employee {

	
	String Empname;
	Integer EmpId;
	public Employee(String empname, Integer empId) {
		super();
		Empname = empname;
		EmpId = empId;
	}
	@Override
	public String toString() {
		return "\n\n"+"Employee Name is "+Empname +"\n"+"Employee id is " + EmpId +"\n\n";
	}
	
	



}
