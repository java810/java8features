package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

import java.util.Scanner;
import java.util.function.Predicate;

public class PredicateEg2 {

	public static void main(String[] args) {
		Predicate<Employee> p = e -> e.EmpId > 1000 && e.Empname.equalsIgnoreCase("sneha");
		// Take input from user
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter  Emp Id ");
		int x = sc.nextInt();

		System.out.print("Enter  Emp Name");
		String y = sc.next();
		if (p.test(new Employee(y, x))) {
			System.out.println("found");
		} else {
			System.out.println("not found");
		}
		sc.close();
	}

}
