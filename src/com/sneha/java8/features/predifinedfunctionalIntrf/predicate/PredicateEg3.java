package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

import java.util.Scanner;
import java.util.function.Predicate;

/**
 * 
 * Write a Predicate to check whether length of string is greater than 5 or not
 */
public class PredicateEg3 {

	public static void main(String[] args) {
		Predicate<String> p = s -> {
			if (s.length() > 5) {
				System.out.println("Length is greater than 5");
			} else {
				System.out.println("Length is less than or equal to 5");
			}
			return false;

		};
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a string ");
		String s= sc.next();
		sc.close();
		p.test(s);
	}
}
