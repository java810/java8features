package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

import java.util.function.Predicate;

/**
 * 1.If we want to check two predicate for same value use join
 * 
 * e.g:  p1 ---> to check whether the number is even or not 
 *       p2 ---> to check whether the number > 10  or not 
 *       
 *       *to check both condition p1.and(p2).test(34);
 *       *to check either true condition p1.or(p2).test(34);
 *       *opposite of p1.negate()
 *
 */
public class PredicateJoning {

	public static void main(String[] args) {
		int[] x= {0,5,10,15,20,25,30,35};
		Predicate<Integer> p1 = i->i%2==0;
		Predicate<Integer> p2 = i->i>10;
		
		//and(),or(),negate()
		System.out.println("The numbers which are even and > 10 are:");
		for(int i:x) {
			if(p1.and(p2).test(i)) {
				System.out.println(i);
			}
		}
		System.out.println("The numbers which are even :");
		for(int i:x) {
			if(p1.test(i)) {
				System.out.println(i);
			}
		}
		
		System.out.println("The numbers which are even or > 10 are:");
		for(int i:x) {
			if(p1.or(p2).test(i)) {
				System.out.println(i);
			}
		}
		System.out.println("The numbers which not are even :");
		for(int i:x) {
			if(p1.negate().test(i)) {
				System.out.println(i);
			}
		}

	}

}
