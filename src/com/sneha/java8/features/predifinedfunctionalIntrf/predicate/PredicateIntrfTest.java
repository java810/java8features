package com.sneha.java8.features.predifinedfunctionalIntrf.predicate;

import java.util.Scanner;
import java.util.function.Predicate;

/**
 * 1.Predicate used for conditional checking
 *
 */
public class PredicateIntrfTest {

	public static void main(String[] args) {
		Predicate<Integer> p = i -> {
			if (i % 2 == 0) {
				System.out.println("even");
				return true;
			} else {
				System.out.println("odd");
				return false;
			}
		};
		       //Take input from user
				Scanner sc= new Scanner(System.in);    
				System.out.print("Enter  number to  Check Even or Odd- ");  
				int x= sc.nextInt();  
		        p.test(x);
		
	}

}
