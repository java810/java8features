package com.sneha.java8.features.predifinedfunctionalIntrf.primitiveconsumertypes;

import java.util.function.DoubleConsumer;

/**
 *Accepts Double value- void return type
 *method- accept(double)
 *
 */
public class Example3_DoubleConsumer {

	public static void main(String[] args) {
		
		DoubleConsumer dc= i->System.out.println("double value "+i);
		dc.accept(100.00);

	}

}
