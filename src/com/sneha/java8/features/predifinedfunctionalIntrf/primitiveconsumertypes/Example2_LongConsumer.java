package com.sneha.java8.features.predifinedfunctionalIntrf.primitiveconsumertypes;

import java.util.function.LongConsumer;

/**
 *Accepts long value- void return type
 *method- accept(long)
 *
 */
public class Example2_LongConsumer {

	public static void main(String[] args) {
		LongConsumer lc= i->{
			long a=10;
			long b=a+i;
			System.out.println("New Long value "+b);
			
		};
		lc.accept(10);

	}

}
