package com.sneha.java8.features.predifinedfunctionalIntrf.primitiveconsumertypes;

import java.util.function.ObjDoubleConsumer;

/**
 * return type is void 
 * Two input is accepted -one of object type(any type) and another input is compulsory a double value
 * The object type must be specified explicitly through arguments
 *
 */
public class Example4_ObjDoubleConsumer {

	public static void main(String[] args) {
		
		ObjDoubleConsumer<Integer> odc = (a,b)->System.out.println("Object value is "+a+"\nDouble value is "+b);
		odc.accept(10, 100.00);

	}

}