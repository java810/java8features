package com.sneha.java8.features.predifinedfunctionalIntrf.primitiveconsumertypes;

import java.util.function.ObjLongConsumer;

/**
 * return type is void 
 * Two input is accepted -one of object type(any type) and another input is compulsory a long value
 * The object type must be specified explicitly through arguments
 *
 */
public class Example5_ObjLongConsumer {

	public static void main(String[] args) {

		ObjLongConsumer<Double> olc = (a, b) -> System.out
				.println("Object value of double type is " + a + "\nLong value is " + b);
		olc.accept(100.00, 100);

	}

}
