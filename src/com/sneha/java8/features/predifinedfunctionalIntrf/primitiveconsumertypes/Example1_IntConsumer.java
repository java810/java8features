package com.sneha.java8.features.predifinedfunctionalIntrf.primitiveconsumertypes;

import java.util.function.IntConsumer;

/**
 *Accepts int value- void return type
 *method- accept(int)
 *
 */
public class Example1_IntConsumer {

	public static void main(String[] args) {
		IntConsumer ic= i->System.out.println("Im int consumer with input value "+i);
		 ic.accept(10);
	}

}
