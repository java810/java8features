package com.sneha.java8.features.predifinedfunctionalIntrf.consumer;

import java.util.function.Consumer;

/**
 *Write a consumer example to print our own custom class
 *
 */
public class Example2 {

	public static void main(String[] args) {

		Consumer<Student> c = s -> System.out
				.println("Name of Student: " + s.name + "\n" + "Marks of Student: " + s.marks);
		c.accept(new Student("Sneha", 90));
	}

}
