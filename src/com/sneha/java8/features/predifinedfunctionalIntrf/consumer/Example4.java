package com.sneha.java8.features.predifinedfunctionalIntrf.consumer;

import java.util.function.Consumer;

/**
 * Consumer chaining is also possible
 * Note: There is no compose method in consumer class
 *
 */
public class Example4 {

	public static void main(String[] args) {

		Consumer<String> c1 = s -> System.out.println(s + " Im first consumer");
		Consumer<String> c2 = s -> System.out.println(s + " Im Second consumer");
		Consumer<String> c3 = s -> System.out.println(s + " Im Third consumer");

		// first way
		c1.andThen(c2).andThen(c3).accept("kaushi");

		// Second way
		Consumer<String> cc = c1.andThen(c2).andThen(c3);
		cc.accept("sneha");

	}

}
