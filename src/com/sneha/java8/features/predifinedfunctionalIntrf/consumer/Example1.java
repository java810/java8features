package com.sneha.java8.features.predifinedfunctionalIntrf.consumer;

import java.util.function.Consumer;

/**
 * Consumer<T>-- returns nothing void return type
 * only takes input
 * used for printing purpose
 *
 */
public class Example1 {

	public static void main(String[] args) {
	Consumer<String> c= s->System.out.println(s);
	c.accept("Print me");

	}

}
