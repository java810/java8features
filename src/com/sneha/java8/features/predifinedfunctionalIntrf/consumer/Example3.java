package com.sneha.java8.features.predifinedfunctionalIntrf.consumer;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * 1.Function can be applied to our own custom classes
 * 2.wap to return grade of the student based on the marks
 * 3.Print using consumer
 *  >=80--A
 *  >=60--B
 *  >=50--C
 *  >=35--D
 *
 */
public class Example3 {

	public static void main(String[] args) {
		Function<Student, String> f = s -> {
			String grade = "";
			if (s.marks >= 80)
				grade = "A";
			else if (s.marks >= 60 && s.marks < 80)
				grade = "B";
			else if (s.marks >= 50 && s.marks < 60)
				grade = "C";
			else if (s.marks >= 35 && s.marks < 50)
				grade = "B";
			else
				grade = "failed";
			return grade;
		};

		/*----------------------------------------------------------------------------------------*/
		

		/* take a student array and print all the student with grade B and their grade */
		Student[] student = { new Student("Sneha", 79), new Student("Kauslendra", 65), new Student("Pooja", 35),
				new Student("Sanket", 50), new Student("Siyali", 70) };
		Predicate<Student> p = s -> {
			if (s.marks >= 60 && s.marks < 80) {
				System.out.println("");
				System.out.println(s.name+" "+f.apply(s));
				System.out.println("");
				
			}
			return false;
		};

		for (Student s : student) {
			p.test(s);
		}
		
		
		/* ------------------------------------------------------------*/
		
		
		/*Print grade of every student*/
		Consumer<Student[]> c= su->{
			for(Student s:student) {
				System.out.println("Student name :"+s.name+"\n"+"Student Grade :"+f.apply(s));
			}
		};
		c.accept(student);

	}

}
