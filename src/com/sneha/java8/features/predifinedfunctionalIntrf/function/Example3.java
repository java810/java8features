package com.sneha.java8.features.predifinedfunctionalIntrf.function;

import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * 1.Function can be applied to our own custom classes
 * 2.wap to return grade of the student based on the marks
 *  >=80--A
 *  >=60--B
 *  >=50--C
 *  >=35--D
 *
 */
public class Example3 {

	public static void main(String[] args) {
		Function<Student, String> f = s -> {
			String grade = "";
			if (s.marks >= 80)
				grade = "A";
			else if (s.marks >= 60 && s.marks < 80)
				grade = "B";
			else if (s.marks >= 50 && s.marks < 60)
				grade = "C";
			else if (s.marks >= 35 && s.marks < 50)
				grade = "B";
			else
				grade = "failed";
			return grade;
		};

		// Taking Input From User
		Scanner sc = new Scanner(System.in);
		System.out.println("enter student name:");
		String name = sc.next();
		System.out.println("enter student marks:");
		int marks = sc.nextInt();
		String grade = f.apply(new Student(name, marks));
		System.out.println("Grade of " + name + " is " + grade);
		sc.close();

		/* take a student array and print all the student with grade B and their grade */
		Student[] student = { new Student("Sneha", 79), new Student("Kauslendra", 65), new Student("Pooja", 35),
				new Student("Sanket", 50), new Student("Siyali", 70) };
		Predicate<Student> p = s -> {
			if (s.marks >= 60 && s.marks < 80) {
				System.out.println("");
				System.out.println(s.name+" "+f.apply(s));
				
			}
			return false;
		};

		for (Student s : student) {
			p.test(s);
		}

	}

}
