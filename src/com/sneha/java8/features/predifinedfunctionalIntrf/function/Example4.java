package com.sneha.java8.features.predifinedfunctionalIntrf.function;

import java.util.Scanner;
import java.util.function.Function;

/**
 * 1.Function Chaining-multiple function can be changed as below:
 * 
 * eg:f1.andThen(f2).apply(i) 
 * Above first f1 will be applied,then f2 will be applied on i
 * 
 * f1.compose(f2).apply(i)
 * Above first f2 will be applied ,then f1 will be applied on i
 * 
 *
 */
public class Example4 {

	public static void main(String[] args) {

		Function<Integer, Integer> f1 = i -> 2 * i;
		Function<Integer, Integer> f2 = i -> i * i;
		// first apply f1 then f2
		System.out.println(f1.andThen(f2).apply(3));
		// first apply f2 then f1
		System.out.println(f1.compose(f2).apply(3));

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the input:");
		int i = sc.nextInt();
		// first apply f1 then f2
		System.out.println("first apply f1 then f2,Result: "+f1.andThen(f2).apply(i));
		// first apply f2 then f1
		System.out.println("first apply f2 then f1,Result: "+f1.compose(f2).apply(i));
		sc.close();

	}

}
