package com.sneha.java8.features.predifinedfunctionalIntrf.function;

import java.util.function.Function;

/**
 * 1.Return Type needs to be specified 
 * 2.apply method is the predefined method
 * 3.take integer and return square of the integer
 *
 */
public class Example1 {

	public static void main(String[] args) {
		Function<Integer,Integer> f= i->i*i;
		System.out.println(f.apply(4));

	}

}
