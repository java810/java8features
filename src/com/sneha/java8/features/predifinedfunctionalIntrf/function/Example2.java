package com.sneha.java8.features.predifinedfunctionalIntrf.function;

import java.util.Scanner;
import java.util.function.Function;

/**
 * 1.WAP to take input as string and find the length of the string
 *
 */
public class Example2 {

	
	public static void main(String[] args) {
		Function<String, Integer> f= s-> s.length();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the String:");
		String s= sc.next();
		System.out.println(f.apply(s));
		sc.close();

	}

}
