package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.IntToDoubleFunction;

/**
 * Takes input type as - Int 
 * Return type is -Double
 * Hence no need to specify return type
 * applyAsDouble- method
 *
 */
public class Example6_IntToDoubleFunction {

	public static void main(String[] args) {
		IntToDoubleFunction id = i -> i * i;
		System.out.println(id.applyAsDouble(100));

	}

}
