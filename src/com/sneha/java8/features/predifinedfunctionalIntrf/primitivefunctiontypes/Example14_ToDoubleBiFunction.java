package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToDoubleBiFunction;

/**
 * two inputs of any type but return type is Double method applyAsDouble
 *
 */
public class Example14_ToDoubleBiFunction {

	public static void main(String[] args) {
		// Input is two double values a , b which im specifying through parameters
		// explicitly
		ToDoubleBiFunction<Double, Double> dbf = (a, b) -> a + b;

		// Output is by default double no need to specify
		System.out.println(dbf.applyAsDouble(100.0, 100.0));

	}

}
