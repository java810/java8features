package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.DoubleFunction;

/**
 *DoubleFunction 
 *1.can take input type as double 
 *2.but return type can be of any type
 *3.hence ,return type needs to specified explicitly
 *
 */
public class Example1_DoubleFunction {

	public static void main(String[] args) {
		DoubleFunction<Double> df= i->i*i;
		System.out.println(df.apply(90));

	}

}
