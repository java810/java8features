package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.DoubleToLongFunction;

/**
 * Takes input type as - Double 
 * Return type is -long
 * Hence no need to specify return type
 * applyAsLong- method
 *
 */
public class Example5_DoubleToLongFunction {

	public static void main(String[] args) {
	DoubleToLongFunction dl= i ->{
		System.out.println(i*i);
		return 922799898;
	};
	dl.applyAsLong(10);
	}

}
