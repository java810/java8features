package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToIntBiFunction;

/**
 * two inputs of any type but return type is int method applyAsInt
 *
 */
public class Example15_ToIntBiFunction {

	public static void main(String[] args) {

		ToIntBiFunction<Integer, Integer> ibf = (a, b) -> a * b;
		System.out.println(ibf.applyAsInt(10, 300));

	}

}
