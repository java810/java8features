package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToLongFunction;

/**
 * return type is long but input can be anything method- applyAsLong(T value)
 */
public class Example12_ToLongFunction {

	public static void main(String[] args) {
		ToLongFunction<Integer> tl= i->i*i;
		System.out.println(tl.applyAsLong(10));

	}
}
