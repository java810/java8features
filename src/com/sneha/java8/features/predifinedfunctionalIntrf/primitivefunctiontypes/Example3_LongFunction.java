package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.LongFunction;

/**
 *LongFunction 
 *1.can take input type as Long 
 *2.but return type can be of any type
 *3.hence ,return type needs to specified explicitly
 *
 */
public class Example3_LongFunction {

	public static void main(String[] args) {
		
		LongFunction<Long> lf= i->i*i;
		System.out.println(lf.apply(90));

	}

}
