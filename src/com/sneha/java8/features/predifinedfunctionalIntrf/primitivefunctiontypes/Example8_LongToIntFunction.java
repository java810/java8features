package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.LongToIntFunction;

/**
 * Takes input type as - Long 
 * Return type is -Int
 * Hence no need to specify return type
 * applyAsInt- method
 *
 */
public class Example8_LongToIntFunction {

	public static void main(String[] args) {

		LongToIntFunction li = i -> (int) i / 10;
		System.out.println(li.applyAsInt(100));
	}

}
