package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToIntFunction;

/**
 * return type is int but input can be anything method- applyAsInt(T value)
 */
public class Example10_ToIntFunction {

	public static void main(String[] args) {

		ToIntFunction<Double> ti = i -> {
			//input is double
			System.out.println(i * i);
			//returning int value
			return 10;
		};
		
		ti.applyAsInt(100.0);
	}

}
