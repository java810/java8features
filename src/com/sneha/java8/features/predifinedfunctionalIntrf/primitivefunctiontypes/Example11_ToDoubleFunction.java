package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToDoubleFunction;

/**
 * return type is Double 
 * but input can be anything
 * method- applyAsDouble(T value)
 */
public class Example11_ToDoubleFunction {

	public static void main(String[] args) {
		ToDoubleFunction<Double> df= i->i*i;
		System.out.println(df.applyAsDouble(100.0));

	}

}
