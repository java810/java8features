package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.IntToLongFunction;

/**
 * Takes input type as - Int 
 * Return type is -Long
 * Hence no need to specify return type
 * applyAsLong- method
 *
 */
public class Example7_IntToLongFunction {

	public static void main(String[] args) {
		
		IntToLongFunction il = i -> i * i;
		System.out.println(il.applyAsLong(10));

	}

}
