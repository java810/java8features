package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.DoubleToIntFunction;

/**
 * Takes input type as - Double 
 * Return type is -int
 * Hence no need to specify return type
 * applyAsInt- method
 *
 */
public class Example4_DoubleToIntFunction {

	public static void main(String[] args) {
		DoubleToIntFunction a= i->{
			System.out.println(i*i);
			return 10;
			
		};
		System.out.println(a.applyAsInt(90));

	}

}
