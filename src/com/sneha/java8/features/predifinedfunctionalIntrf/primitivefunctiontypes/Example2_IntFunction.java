package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.IntFunction;

/**
 *IntFunction 
 *1.can take input type as int 
 *2.but return type can be of any type
 *3.hence ,return type needs to specified explicitly
 *
 */
public class Example2_IntFunction {

	public static void main(String[] args) {
		IntFunction<Integer> f= i->i*i;
		System.out.println(f.apply(10));
	}

}
