package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.ToLongBiFunction;

/**
 * two inputs of any type but return type is Long
 * method applyAsLong
 *
 */
public class Example13_ToLongBiFunction {

	public static void main(String[] args) {
		//input is two integer a,b
		ToLongBiFunction<Integer,Integer> lbf = (a,b) -> a+b;
		//output is a long value
		System.out.println(lbf.applyAsLong(10, 10));

	}

}
