package com.sneha.java8.features.predifinedfunctionalIntrf.primitivefunctiontypes;

import java.util.function.LongToDoubleFunction;

/**
 * Takes input type as - Long 
 * Return type is -Double
 * Hence no need to specify return type
 * applyAsDouble- method
 *
 */
public class Example9_LongToDoubleFunction {

	public static void main(String[] args) {
		
		LongToDoubleFunction ld= i->i*i;
		System.out.println(ld.applyAsDouble(100));

	}

}
