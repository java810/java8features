package com.sneha.java8.features.predifinedfunctionalIntrf.supplier;

import java.util.function.Supplier;

/**
 * Generation of random otp
 *
 */
public class Example2 {

	public static void main(String[] args) {
		Supplier<String> s = () -> {
			String otp = "";
			for (int i = 0; i < 6; i++) {
				otp = otp + (int) (Math.random() * 10);
			}
			return otp;
		};
		
		System.out.println("Random otp of 6 digit: " + s.get());
		System.out.println("Random otp of 6 digit: " + s.get());
	}

}
