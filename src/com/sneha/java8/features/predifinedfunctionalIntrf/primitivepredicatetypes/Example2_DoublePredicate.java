package com.sneha.java8.features.predifinedfunctionalIntrf.primitivepredicatetypes;

import java.util.function.DoublePredicate;

/**
 * 1.Predicate<Double> does autoboxing and unboxing 
 * 2.Hence ,Performance Impact 
 * 3.To overcome ,go for DoublePredicate 
 * 4.No need to explicitly specify the type by default takes Double
 *
 */
public class Example2_DoublePredicate {

	public static void main(String[] args) {
		DoublePredicate dp= i->i%2==0;
		System.out.println(dp.test(1.7));

	}

}
