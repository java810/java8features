package com.sneha.java8.features.predifinedfunctionalIntrf.primitivepredicatetypes;

import java.util.function.LongPredicate;

/**
 * 1.Predicate<Long> does autoboxing and unboxing 
 * 2.Hence ,Performance Impact 
 * 3.To overcome ,go for LongPredicate 
 * 4.No need to explicitly specify the type by default takes Long
 *
 */
public class Example3_LongPredicate {
	
	public static void main(String[] args) {
		
		LongPredicate lp= i->i%2==0;
		System.out.println(lp.test(802));
	}
}
