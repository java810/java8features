package com.sneha.java8.features.predifinedfunctionalIntrf.primitivepredicatetypes;

import java.util.function.IntPredicate;

/**
 * 1.Predicate<Integer> does autoboxing and unboxing 
 * 2.Hence ,Performance Impact 
 * 3.To overcome ,go for IntPredicate 
 * 4.No need to explicitly specify the type by default takes int
 *
 */
public class Example1_IntPredicate {

	public static void main(String[] args) {

		IntPredicate p = i -> i % 2 == 0;
		System.out.println(p.test(10));
	}

}
