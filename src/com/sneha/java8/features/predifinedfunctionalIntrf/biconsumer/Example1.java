package com.sneha.java8.features.predifinedfunctionalIntrf.biconsumer;

import java.util.ArrayList;
import java.util.function.BiConsumer;

/**
 * Biconsumer will take two arguments and perform some operation and wont return
 * any value. Write a program to increment every employee salary by 500
 *
 */
public class Example1 {

	public static void main(String[] args) {

		ArrayList<Employee> l = new ArrayList<Employee>();
		l.add(new Employee("Sneha", 1000));
		l.add(new Employee("Kaushi", 2000));
		l.add(new Employee("Kavita", 3000));
		System.out.println("Before increment salary of employee's:");
		for (Employee e : l) {
			System.out.println(e);
		}
		System.out.println();

		BiConsumer<Employee, Integer> bc = (emp, incremnt) -> emp.Salary = emp.Salary + 500;

		for (Employee e : l) {
			bc.accept(e, 500);
		}

		System.out.println("After increment salary of employee's:");

		for (Employee e : l) {
			System.out.println(e);
		}
	}

}
