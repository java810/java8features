package com.sneha.java8.features.predifinedfunctionalIntrf.biconsumer;

public class Employee {

	String Empname;
	Integer Salary;

	public Employee(String Empname, Integer Salary) {
		
		this.Empname = Empname;
		this.Salary = Salary;
	}

	@Override
	public String toString() {
		return "Employee [Empname=" + Empname + ", Salary=" + Salary + "]";
	}

}
