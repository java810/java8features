package com.sneha.java8.features;

public class Test3 {

	public static void main(String[] args) {
		Example7 example7 = new Example7();
		//this also can be written as :
		FunctionInterfaceLamdaExpressionEg3 i= new Example7();/*Parent holding child reference*/
		
		System.out.println(example7.squareit(2));
		System.out.println(i.squareit(3));
	}

}
