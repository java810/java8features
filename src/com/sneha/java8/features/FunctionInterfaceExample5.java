package com.sneha.java8.features;

public interface FunctionInterfaceExample5 extends FunctionInterfaceExample1 {
	
	
	
	
	public void m2();
	
	//Perfectly valid as though  child class has two abstract  methods but it is not a functional Interface 
			/*
			 * Method name is different in parent and child
			 * hence child has two abstract methods,and parent has one abstract method 
			 * hence parent is  a functional Interface as it is annotated @Functional Interface and has one SAM
			 * and child is not annotated @Functional Interface it can have more than one abstract method
			 */

}
