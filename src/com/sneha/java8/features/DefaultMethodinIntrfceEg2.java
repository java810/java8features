package com.sneha.java8.features;

/*
 * Object class method already available to the implementation class ,therefore we do not require to make it available through default method.
 * 
 * --"Hence ,Object class method we can't  implement as default Method"
 * 
 * --We will get compile time error
 */
public interface DefaultMethodinIntrfceEg2 {
	/*
	 * default int hashCode() { return 10; }
	 */

	// Above is compile time error ---"Default method hashCode in interface
	// DefaultMethodinIntrfceEg2 overrides a member of java.lang.Object
}
