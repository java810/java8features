package com.sneha.java8.features.staticmethodsinIntrf;

/**
 * 1.Since Main method is static ,therefore we can declare main method inside interface for Java 8 onwards
 * 2.Hence we can directly run the interface with main method
 * 3.If everything is static nowhere related to object take inside interface automatically performance will increase why create a class
 *
 */
public interface Interfwithmainmethod {
	
	public static void main(String[] args) {
		System.out.println("Main Staic Method in interface");
	}

}
