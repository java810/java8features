package com.sneha.java8.features.staticmethodsinIntrf;

/**
 * 1.Interface can have any number of static methods from Java 8 Onwards
 *
 */
public interface Interfwithstaticmethods {

	public static void m1() {
		System.out.println("Interface Static Method 1");
	}
	
	public static void m2() {
		System.out.println("Interface Static Method 2");
	}

}
