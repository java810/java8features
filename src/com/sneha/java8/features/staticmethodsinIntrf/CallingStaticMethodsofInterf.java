package com.sneha.java8.features.staticmethodsinIntrf;

/**
 * How to call static methods of Interface
 * 1.Interface static methods by default not available to the implementation classes
 * 2.It can be called by using Interface name only
 *
 */
public class CallingStaticMethodsofInterf implements Interfwithstaticmethods{

	public static void main(String[] args) {
		
        //Interface name.method name
		Interfwithstaticmethods.m1();
		Interfwithstaticmethods.m2();
		
		
	}

}
