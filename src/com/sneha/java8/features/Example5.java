package com.sneha.java8.features;
//Second example of without lamda----When there was no lamda expression
//addition of numbers --invoked by Test2.java
public class Example5 implements FunctionInterfaceExample2{

	@Override
	public void m2(int a,int b) {
		
		System.out.println("sum is "+(a+b));
	}

}
