package com.sneha.java8.features;
/*
 * --1)Inside LamdaExpression we can't declare instance variables
 * --2)Whatever variables declared inside LamdaExpression are considered as local variables.
 * --3)Within LamdaExpression "this" keyword represents current outer class object reference(that is
 *     current enclosing class reference in which we have declared lambda expression.
 */
public class LamdaExpressionVariablesConceptEg  {

	int x= 777;
	public void m2() {
		FunctionInterfaceExample1 i= ()->{
			int x=888;
			System.out.println(x);//o/p=888
			System.out.println(this.x);//o/p=777
		};
		
		i.m1();
		

	}
	public static void main(String[] args) {
		LamdaExpressionVariablesConceptEg t = new LamdaExpressionVariablesConceptEg();
		t.m2();
	}
}
