package com.sneha.java8.features;

/*
 *-- 1)AnonymousInnerClass is more powerful than Lambda Expression
 *
 *-- 2)If  AnonymousInnerClass implements an interface that contains single abstract method 
 *      then only we can replace that AnonymousInnerClass with lambda expression
 *  
 *-- 3)Lambda expression can always we replaced by AnonymousInnerClass
 *
 *-- 4) AnonymousInnerClass != lambda expression
 *
 *-- 5)AnonymousInnerClass can be used where more the one abstract method is there 
 *      e.g:
 *      interface A{
 *      m1();
 *      m2();
 *      }
 *      A a = new A(){
 *      public void m1(){
 *      //First method
 *      }
 *      public void m2(){
 *      //Second method
 *      }
 *      };
 *
 */
public class AnonymousInnerClassEg {

	public static void main(String[] args) {

		/*
		 * Instead of taking separate implementation class(eg:Myrunnable) wherever the
		 * functionality is required there only i'm defining AnonymousInnerClass
		 */

		Runnable r = new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 20; i++) {
					System.out.println("Child Thread-2");
				}

			}
		};
		/*
		 * Above code ,below activities i'm doing: 1)I'm writing a class that implements
		 * runnable 2)Proving implementation for run method 3)And this implementation
		 * class i'm creating an object by using new Runnable()
		 * 
		 * AnonymousInnerClass is for instance use 
		 */

		Thread t = new Thread(r);
		t.start();
		for (int i = 0; i < 20; i++) {
			System.out.println("Main Thread-2");
		}

	}
/* 
 *Here we can AnonymousInnerClass replace with Lambda Expression as Runnable has single abstract method.
 *Please refer-   MultiThreadusingRunnableIntrfEgwithLamdaExp.java  for the same
 */
}
