package com.sneha.java8.features;
@FunctionalInterface
public interface FunctionInterfaceExample1 {
	//Single Abstract Method
	public void m1();
	
	/*
	 * Notes-- It is not compulsory  to mention @FunctionalInterface on Top of the Interface .
	 * Here ,by mentioning @FunctionalInterface we are explicitly marking  this interface as functional Interface so that we can find compile time issues
	 *  if functional interface rules are violated. 
	 */

}
