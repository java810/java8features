package com.sneha.java8.features;

@FunctionalInterface
public interface FunctionInterfaceExample2 {
	// Single Abstract Method
	public void m2(int a ,int b);

	/*
	 * In functional Interface we can write any number of static and default methods
	 * but we must have single Abstract Method
	 * 
	 */
	default void m3() {

	}

	public static void m4() {

	}

	public static void m5() {

	}

	default void m6() {

	}

}
