package com.sneha.java8.features;

/**
 * 1.Multiple Inheritance with respect to default methods is possible 2.But if
 * both interface have default method with same name then compulsory we need to
 * override the default method by: a)providing our own custom implementation
 * b)or if we want the default implementation then call by
 * Intefacename.super.methodname();
 *
 */
public class MultiInteritnceTest_ForDefaultMethods
		implements MultiInteritncewrtDefaultMethods_Left, MultiInteritncewrtDefaultMethods_Right {

	public static void main(String[] args) {
		MultiInteritnceTest_ForDefaultMethods t = new MultiInteritnceTest_ForDefaultMethods();
		t.m1();

	}

	@Override
	public void m1() {
		// calling default implementation of left method
		MultiInteritncewrtDefaultMethods_Left.super.m1();

		// calling default implementation of Right method
		MultiInteritncewrtDefaultMethods_Right.super.m1();

		// Custom default implementation
		System.out.println("im overriden im MultiInteritnceTest_ForDefaultMethods ");
	}

}
