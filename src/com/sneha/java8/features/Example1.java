package com.sneha.java8.features;

import java.util.function.Function;
//Function
public class Example1 {

	public static void main(String[] args) {
		
		// Function to print square of a number
		Function<Integer, Integer> f = i -> i * i;

		// Using functions Example
		System.out.println("Square of 5 is " + f.apply(5));
		System.out.println("Square of 2 is " + f.apply(2));
		System.out.println("Square of 3 is " + f.apply(3));

	}

}
