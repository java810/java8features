package com.sneha.java8.features;

import java.util.Scanner;
import java.util.function.Predicate;

//Predicate
public class Example2 {

	public static void main(String[] args) {
		//To check if a number is even or not 
	    Predicate<Integer> p= i->i%2==0;
	    
	    //Take input from user
		Scanner sc= new Scanner(System.in);    
		System.out.print("Enter  number to  Check Even or Odd- ");  
		int x= sc.nextInt();  
		
		//Check given input is even or odd
		if(p.test(x)==true)  System.out.println("Even Number");
		else System.out.println("Odd Number");

	}

}
