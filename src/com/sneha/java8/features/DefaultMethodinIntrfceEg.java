package com.sneha.java8.features;

//From Java 1.8 onwards we can declare any number of default and static methods in a interface
//I'll use TestDefaultMethod.java to implement this interface
public interface DefaultMethodinIntrfceEg {

	public void m1(); // abstract method

	public void m2(); // abstract method

	default void m3() {
		// Default Method
		System.out.println("Im am default Method m3--This is the default implementation of me");
	}

	default void m4() {
		// Default Method
		System.out.println("Im am default Method m4--This is the default implementation of me");
	}

}
