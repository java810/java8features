package com.sneha.java8.features;
//Lamda Expression Example
//How to do example 7 using lamda expression 
/*
 * 1.For Lambda expression separate dot class file will not be generated 
 * 
 */
public class Example8 {
	


	public static void main(String[] args) {
		//no return statement required when not curly braces in lambda expression(single statement)
		FunctionInterfaceLamdaExpressionEg3 eg3= x->x*x;
		System.out.println(eg3.squareit(2));
		System.out.println(eg3.squareit(3));

	}


}
