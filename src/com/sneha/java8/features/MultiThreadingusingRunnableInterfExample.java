package com.sneha.java8.features;
//Thread concept without lambda expression
//This class is invoked using Test4.java
public class MultiThreadingusingRunnableInterfExample implements Runnable{

	@Override
	public void run() {
		for (int i=0; i<10; i++) {
			System.out.println("Child Thread");
		}
	}

}
