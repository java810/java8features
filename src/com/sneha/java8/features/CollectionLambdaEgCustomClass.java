package com.sneha.java8.features;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/*Lambda expression  concept can be used
 * in collection for our own custom classes also other that integer,String.
 * In this example we will use Employee.java class and sort it 
 * 
 * */
public class CollectionLambdaEgCustomClass {
	
	public static void main(String[] args) {
		ArrayList<Employee> l= new ArrayList<Employee>();
		l.add(new Employee("Sneha", 10));
		l.add(new Employee("Kaushi", 20));
		l.add(new Employee("Pranjul", 30));
		l.add(new Employee("Rishab", 40));
		l.add(new Employee("Ankita", 50));	
		System.out.println("Default Insertion Order: "+l);
	/*Writing lambda expression to sort in ascending order of employee id*/
		Comparator<Employee> c = (e1,e2) -> (e1.EmpId<e2.EmpId)?-1:(e1.EmpId>e2.EmpId)?+1:0;
		Collections.sort(l,c); 
		
		/*
		 * above 2 lines 23 ,24 can be combined as :
		 * Collections.sort(l,(e1,e2) -> (e1.EmpId<e2.EmpId)?-1:(e1.EmpId>e2.EmpId)?+1:0);
		 */
		
	    System.out.println("Sorting in ascending Order of EmpId: "+l);
	    
	    /*Writing lambda expression to sort in ascending order of employee name*/
	    /*Default natural sorting order implemented by compareTo method of comparable class eg: for string and integer ascending order*/
		Collections.sort(l,(e1,e2) -> e1.Empname.compareTo(e2.Empname)); 
		 System.out.println("Sorting in ascending Order of EmpName: "+l);
	}

}
