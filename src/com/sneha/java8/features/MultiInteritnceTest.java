package com.sneha.java8.features;

/**
 * Multiple Inheritance is allowed from Java 8 Onwards ,If two in interface has
 * same method name ,overridden implementation will be called
 */
public class MultiInteritnceTest implements MultiInteritnceLeft, MultiInteritnceRight {

	@Override
	public void m1() {
		System.out.println("Im overriden");

	}

	@Override
	public void m2() {
		System.out.println("Im overriden m2 of MultiInteritnceLeft");

	}

	public static void main(String[] args) {
		MultiInteritnceTest t = new MultiInteritnceTest();
		t.m1();
		t.m2();

	}

}
