package com.sneha.java8.features;

//From Java 1.8 onwards we can declare any number of default and static methods in a interface and if the interface is Functional Interface it can have only one abstract method
//I'll use TestDefaultMethodinFuncIntrfImpl.java to implement this interface
@FunctionalInterface
public interface DefaultMethodinFunctionalIntrfceEg {

	public void m1();

	// public void m2(); --- Compile time error (SAM-Single Abstract Method if
	// @FunctionalInterface)

	default void m3() {
		// Default Method
		System.out.println("Im am default Method m3--This is the default implementation of me");
	}

	default void m4() {
		// Default Method
		System.out.println("Im am default Method m4--This is the default implementation of me");
	}
}
