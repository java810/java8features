package com.sneha.java8.features;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CollectionwithLambdaExample {

	public static void main(String[] args) {
		ArrayList<Integer> l= new ArrayList<Integer>();
		l.add(200);
		l.add(100);
		l.add(300);
		l.add(500);
		System.out.println("Default Insertion Order: "+l);
	    //We are using lambda expression instead of MyComparator.java class as Comparator Interface is a functional interface have only one abstract method -compare()
		Comparator<Integer> c = (I1,I2)->(I1 < I2)?-1:(I1 > I2)?+1:0;
		Collections.sort(l, c); //Passing list and Reference c;
		System.out.println("Sorting in acending Order: "+l);

	}

}
